//****************************************BUDGET CONTROLLER

var budgetcontroller = (function(){
 	var Expense = function(id, description, value){
 		this.id = id;
 		this.description = description;
 		this.value = value;
 	}
 	var Income = function(id, description, value){
 		this.id = id;
 		this.description = description;
 		this.value = value;
 	}

 	var data = {
 		allItems : {
 			exp : [],
 			inc : []  
 		},
 		totals : {
 			exo : 0,
 			inc : 0
 		},
 		budget : 0,
 		percentage : -1
 	}

 	var caluclateTotal = function(type){
 		var sum = 0;
 		data.allItems[type].forEach(function(cur){
 			sum += cur.value;
 		})
 			data.totals[type] = sum;
 	}
 	return {
		addItem : function(type,des,val){
			var newItem,ID;
			if(data.allItems[type].length > 0){
				ID = data.allItems[type][data.allItems[type].length-1].id + 1;
			}else{
				ID = 0;
			}			

			// Create New item bese inc or Exp
			if(type==='inc'){
				newItem = new Income(ID,des,val);
			}else if(type === 'exp'){
				newItem = new Expense(ID,des,val);
			}
			// Push it into our datastructure
			data.allItems[type].push(newItem);
			// Retrun new element
			return newItem;
		},
 		
		deleteItem : function(type,id){
			var ids = data.allItems[type].map(function(current){
				return current.id;
			})
			var index = ids.indexOf(id);
			if(index !== -1){
				data.allItems[type].splice(index,1);
			}  
		},

 		caluclateBudget : function(){
 			// 1 . caluclate total income and total expense
 			caluclateTotal('exp');
 			caluclateTotal('inc');
 			// 2 . caluclate budget : income - expense
 			data.budget = data.totals.inc - data.totals.exp;
 			// 3 . caluclate the percentage of income that we spent 
 			if (data.totals.inc > 0) {
 				data.percentage = Math.round((data.totals.exp / data.totals.inc) * 100);
 			}else{
 				data.percentage = -1;
 			}
 				
 		},
 		getBudget : function(){
 			return{
 				budget : data.budget,
 				totalInc : data.totals.inc,
 				totalExp : data.totals.exp,
 				percentage : data.percentage
 			}
 		},
 		test :function(){
 			console.log(data.allItems);
 			return data.allItems;

 		}
 	}

})();

//***********************************************UI CONTROLLER

var UIcontroller =(function(){
	var DOMString = {
		inputType : '.add__type',
		inputDescription : '.add__description',
		inputValue : '.add__value',
		inputBtn : '.add__btn',
		incomeElement : '.income__list',
		expensesElement : '.expenses__list',
		budgetlabel : '.budget__value',
		incomelabel : '.budget__income--value',
		expenselabel : '.budget__expenses--value',
		percentagelabel : '.budget__expenses--percentage',
		container : '.container'
	}
	
	return {
		getInput : function(){
			return {
				type : document.querySelector(DOMString.inputType).value,
				description : document.querySelector(DOMString.inputDescription).value,
				value : parseFloat(document.querySelector(DOMString.inputValue).value)

			}
		},
		getDOMString : function(){
			return DOMString;
		},
		addlistItem : function(obj,type){
			var html,newHTMl,element;
			//create HTML String Whit placeholder Test
			if (type === 'inc') {
				element = DOMString.incomeElement;
				html = '<div class="item clearfix" id="in-%id%"> <div class="item__description">%description%</div> <div class="right clearfix"> <div class="item__value">+ %value%</div> <div class="item__percentage">75%</div> <div class="item__delete"> <button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button></div></div></div>'

			}else if (type === 'exp') {
				element = DOMString.expensesElement;
				   	html = '<div class="item clearfix" id="exp-%id%"> <div class="item__description">%description%</div> <div class="right clearfix"> <div class="item__value">- %value%</div> <div class="item__percentage">21%</div> <div class="item__delete"> <button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button> </div></div></div>'
			}			
			//Replace place Holder Text Whit Actual data 
			newHTMl = html.replace('%id%', obj.id);
			newHTMl = newHTMl.replace('%description%', obj.description);
			newHTMl = newHTMl.replace('%value%', obj.value);
			//Insert HTML int the DOM
			document.querySelector(element).insertAdjacentHTML('beforeend', newHTMl);
		},
		clearFields : function(){
			var fields,fieldsArr;
			fields = document.querySelectorAll(DOMString.inputDescription+','+DOMString.inputValue);
			

			fieldsArr = Array.prototype.slice.call(fields);
			fieldsArr.forEach( function(current,index,array) {
				current.value = '';
			})
			fieldsArr[0].focus();
		},
		displayBudget : function(obj){
			document.querySelector(DOMString.budgetlabel).textContent = obj.budget;
			document.querySelector(DOMString.incomelabel).textContent = obj.totalInc;
			document.querySelector(DOMString.expenselabel).textContent = obj.totalExp;
			if (obj.percentage > 0) {
				document.querySelector(DOMString.percentagelabel).textContent = obj.percentage + '%';
			}else{
				document.querySelector(DOMString.percentagelabel).textContent = '---';
			}
			
		}
	}
})();

//*******************************************GLOBAL APP CONTROLLER

var controller =(function(budgetCtrl,UICtrl){
	var setupEventlistener = function(){
		var DOM = UICtrl.getDOMString(); 
			document.querySelector(DOM.inputBtn).addEventListener('click',ctrlAddItem); 

			document.addEventListener('keypress',function(event){
			if (event.keyCode === 13 || event.which ===13){
				ctrlAddItem();
			}
		})
			document.querySelector(DOM.container).addEventListener('click',ctrlDeleteItem);
	}

	var updgetBudget = function(){
		// 1. Calculate the budget
		budgetCtrl.caluclateBudget();
		// 2. Return the budget 
		var budget = budgetCtrl.getBudget(); 
		// 3. Display the budget on the UI
		UICtrl.displayBudget(budget);
	}
	var ctrlAddItem = function(){
		var input, newItem;
		// 1. Get the field input data
		input = UICtrl.getInput();
		if (input.description !== '' && !isNaN(input.value) && input.value > 0) {
			// 2. Add the item to the budget controller
			newItem = budgetCtrl.addItem(input.type,input.description,input.value);
			// 3. Add the item to the UI
			UICtrl.addlistItem(newItem,input.type);
			// 4. Clear fields
			UICtrl.clearFields();
			// 5. Calculate and Update Budget
			updgetBudget();
		}
	}
	var ctrlDeleteItem = function(event){
		var itemID;
		itemID = event.target.parentNode.parentNode.parentNode.parentNode.id;
		if (itemID) {
			splidID = itemID.split('-');
			type = splidID [0];
			ID = parseInt(splidID[1]);


			// delete the item from datastructure
			budgetCtrl.deleteItem(type,ID);
			// delete the item from UI
			// Update and Show the New Budget
		}
	}
		return {
			init : function(){
				UICtrl.displayBudget({
					budget : 0,
					totalInc : 0,
					totalExp : 0,
					percentage : -1
				});
				setupEventlistener();
			}
		}

})(budgetcontroller,UIcontroller);

controller.init();